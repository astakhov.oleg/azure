// Kubeconfig
resource "gitlab_project_variable" "backend-kubeconfig" {
  key = "KUBERNETES_CONFIG"
  variable_type = "file"
  project = var.backend_project_id
  value = azurerm_kubernetes_cluster.k8s.kube_config_raw
}

resource "gitlab_project_variable" "frontend-kubeconfig" {
  key = "KUBERNETES_CONFIG"
  variable_type = "file"
  project = var.frontend_project_id
  value = azurerm_kubernetes_cluster.k8s.kube_config_raw
}

// Namespaces
resource "kubernetes_namespace" "app-namespace" {
  for_each = var.environments
  metadata {
    name = each.value.namespace
  }
}

resource "gitlab_project_variable" "backend-namespace" {
  for_each = kubernetes_namespace.app-namespace
  key = "KUBERNETES_NAMESPACE"
  project = var.backend_project_id
  value = each.value.metadata[0].name
  environment_scope = each.key
}

resource "gitlab_project_variable" "frontend-namespace" {
  for_each = kubernetes_namespace.app-namespace
  key = "KUBERNETES_NAMESPACE"
  project = var.frontend_project_id
  value = each.value.metadata[0].name
  environment_scope = each.key
}

// Docker pull secrets
resource "kubernetes_secret" "docker-config" {
  for_each = kubernetes_namespace.app-namespace
  metadata {
    name = "docker-config"
    namespace = each.value.metadata[0].name
  }

  data = {
    ".dockerconfigjson" = <<DOCKER
{
  "auths": {
    "${var.registry_server}": {
      "auth": "${base64encode("${var.registry_username}:${var.registry_password}")}"
    }
  }
}
DOCKER
  }

  type = "kubernetes.io/dockerconfigjson"
}

//App records
resource "cloudflare_record" "app-records" {
  for_each = var.environments
  name = each.value.subdomain
  type = "A"
  zone_id = var.cloudflare_zone_id
  value = data.kubernetes_service.ingress-nginx-service.status.0.load_balancer.0.ingress.0.ip
  proxied = true
}
